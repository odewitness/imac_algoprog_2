#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
    liste->dernier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier == nullptr)
    {
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveau_noeud = (Noeud*) malloc(sizeof(Noeud));

    nouveau_noeud->donnee=valeur;
    nouveau_noeud->suivant=nullptr;

    if(est_vide(liste))
    {
        liste->premier = nouveau_noeud;
        liste->dernier = nouveau_noeud;
    }
    else
    {
        while(liste->dernier->suivant != nullptr)
        {
            liste->dernier = liste->dernier->suivant;
        }
        liste->dernier->suivant = nouveau_noeud;
    }
}

void affiche(const Liste* liste)
{
   if(est_vide(liste))
   {
       cout << "La liste est vide!" << endl;
   }

   Noeud* noeud_actuel = liste->premier;
   int compteurAffichage=0;
   while(noeud_actuel != NULL)
   {
       cout << "Affichage liste n°" << compteurAffichage << " : " << noeud_actuel->donnee << endl;
       noeud_actuel=noeud_actuel->suivant;
       compteurAffichage++;
   }
}

int recupere(const Liste* liste, int n)
{
    Noeud* noeud_actuel = liste->premier;
    int compteur = 0;

    while(noeud_actuel->suivant != nullptr && compteur<n)
    {
        noeud_actuel = noeud_actuel->suivant;
        compteur++;
    }

    if(noeud_actuel->suivant==NULL)
    {
        cout << "Erreur" << endl;
    }

    return noeud_actuel->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud *noeud_actuel = liste->premier;
    int position=0;

    while(noeud_actuel != NULL && noeud_actuel->donnee !=valeur)
    {
        noeud_actuel=noeud_actuel->suivant;
        position++;

        if(noeud_actuel == NULL)
        {
            return -1;
        }     
    }
    return position;        
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* noeud_actuel = liste->premier;
    int compteur=0;

    while(compteur<n)
    {
        noeud_actuel = noeud_actuel->suivant;
        compteur++;
    }

    if(noeud_actuel == NULL)
    {
        cout << "Erreur" << endl;
    }
    
    noeud_actuel->donnee=valeur;
}

void initialise(DynaTableau* tableau, int capacite)
{
    tableau->taille=0;
    tableau->capacite=capacite;
    tableau->donnees=(int*)malloc(sizeof(int)*capacite);
}

bool est_vide(const DynaTableau* liste)
{
    if (liste->taille==0)
    {
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->taille; i++)
    {
        cout << "Affichage tableau dynamique n°" << i << " :" << tableau->donnees[i] << endl;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->taille < tableau->capacite)
    {
        tableau->donnees[tableau->taille] = valeur;
        tableau -> taille++;
    }
    else
    {
        tableau->capacite++;
        int* variableStockage=(int*)realloc(tableau->donnees, tableau->capacite*sizeof(int));
        if(variableStockage==NULL)
        {
            cout << "Erreur" << endl;
        }
        else
        {
            tableau->donnees = variableStockage;
            tableau->donnees[tableau->taille]=valeur;
            tableau->taille++;
        }
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>tableau->taille)
    {
        cout << "Rien a recuperer" << endl;
    }
    else
    {
        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i<tableau->taille; i++)
    {
        if(tableau->donnees[i]==valeur)
        {
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if(liste == NULL)
    {
        exit(EXIT_FAILURE);
    }

    int nombreDefile=0;

    if(liste->premier != NULL)
    {
        Noeud *elementDefile = liste->premier;
        
        nombreDefile = elementDefile->donnee;
        liste->premier = elementDefile ->suivant;
        free (elementDefile);
    }
    return nombreDefile;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if(liste == NULL)
    {
        exit(EXIT_FAILURE);
    }

    int nombreDepile=0;
    Noeud *elementDepile = liste->premier;

    if(liste != NULL && liste->premier != NULL)
    {
        nombreDepile = elementDepile->donnee;
        liste->premier = elementDepile ->suivant;
        free (elementDepile);
    }
    return nombreDepile;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;

    std::cout << std::endl;

    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "4e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "4e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    std::cout << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
