#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
	// retourne l’index du fils gauche du nœud à l’indice nodeIndex
    return ((2 * nodeIndex) + 1);
}

int Heap::rightChild(int nodeIndex)
{
	// retourne l’index du fils droit du nœud à l’indice nodeIndex
    return ((2 * nodeIndex) + 2);
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// insère un nouveau noeud dans le tas heap tout en gardant la propriété de tas
	
	// use (*this)[i] or this->get(i) to get a value at index i

	// (*this) = heap

	int i = heapSize;
	(*this)[i] = value;
	int variableEchange;

	while (i > 0 && (*this)[i] > (*this)[(i-1)/2])
	{
		variableEchange = (*this)[i];
		(*this)[i] = (*this)[(i-1)/2];
		(*this)[(i-1)/2] = variableEchange;
		i = (i-1)/2;
	}
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// Si le noeud à l’indice nodeIndex n’est pas supérieur
	// à ses enfants, reconstruit le tas à partir de cette index
	
	// use (*this)[i] or this->get(i) to get a value at index i
	
	int i_max = nodeIndex;
	
	int indexGauche = leftChild(nodeIndex);
	int indexDroite = rightChild(nodeIndex);
	int valeurGauche = -1; 
	int valeurDroite = -1;

	if(indexGauche < heapSize)
	{
		valeurGauche = (*this)[leftChild(nodeIndex)];
	}
	else
	{
		valeurGauche = -1;
	}
	
	if(indexDroite < heapSize)
	{
		valeurDroite = (*this)[rightChild(nodeIndex)];
	}
	else
	{
		valeurDroite = -1;
	}

	if(indexDroite < heapSize && indexGauche < heapSize)
	{
		if((*this)[nodeIndex] < valeurDroite)
		{
			i_max = indexDroite;
		}
		if((*this)[i_max] < valeurGauche)
		{
			i_max = indexGauche;
		}
	}

	// If largest is not root
	if(i_max != nodeIndex)
	{
		swap(nodeIndex, i_max);
		// Recursively heapify the affected sub-tree
		heapify(heapSize, i_max);
	}
}

void Heap::buildHeap(Array& numbers)
{
	// Construit un tas à partir des valeurs de numbers 
	// (vous pouvez utiliser soit insertHeapNode soit heapify)

	int n = numbers.size();
	for (int i=0; i < n; i++)
	{
		insertHeapNode(n, numbers[i]);
	}
	
}

void Heap::heapSort()
{
	// Construit un tableau trié à partir d’un tas heap
	int n = (*this).size();
	for (int i=n-1; i >= 0; i--)
	{
		swap(0,i);
		heapify(i,0);
	}
	
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
