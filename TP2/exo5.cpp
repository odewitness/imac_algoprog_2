#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if (origin.size() <= 1)
	{
		return;
	}

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
	for(uint i = 0; i < first.size(); i++)
	{
		first.set(i, origin[i]);
	}
	for(uint j = 0; j < second.size(); j++)
	{
		second.set(j, origin[first.size()+j]);
	}

	// recursiv splitAndMerge of lowerArray and greaterArray
	splitAndMerge(first);
	splitAndMerge(second);

	// merge
	merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
	int secondIndex = 0;
	int firstIndex = 0;

	while(firstIndex != first.size() || secondIndex != second.size())
	{
		if(firstIndex == first.size())
		{
			result[firstIndex+secondIndex]=second[secondIndex];
			secondIndex++;
		}
		else if(secondIndex == second.size())
		{
			result[firstIndex + secondIndex] = first[firstIndex];
			firstIndex++;
		}
		else if(first[firstIndex] < second[secondIndex])
		{
			result[firstIndex + secondIndex] = first[firstIndex];
			firstIndex++;
		}
		else
		{
			result[firstIndex + secondIndex]=second[secondIndex];
			secondIndex++;
		}
	}
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
