#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void bubbleSort(Array& toSort)
{
	bool flag = 0;
	for(uint i=1; i<toSort.size()-1; i++)
	{
		flag = 0;
		for(uint j=0; j<toSort.size()-i; j++)
		{
			if (toSort[j] > toSort[j+1]) 
			{
				toSort.swap(j,j+1);
				flag = 1;
			}   
		}
		if (flag == 0)
		{
			break;
		}
	}
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
