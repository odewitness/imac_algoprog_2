#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

int binarySearch(Array& array, int toSearch)
{
	int start = 0;
	int end = array.size();
	int foundIndex = -1;
	while (start < end)
	{
		// On initialise le milieu
		int mid = (start+end)/2;

		// Si la valeur recherchée est plus grande que la valeur du milieu
		if (toSearch > array[mid])
		{
			// On se décale vers la droite et le nouveau start est égale à la valeur située après l'ancienne valeur du milieu
			start = mid + 1;
		}
		// Si la valeur recherchée est plus petite que la valeur du milieu
		else if (toSearch < array[mid])
		{
			// On se décale vers la gauche et le nouveau end est égal à l'ancienne valeur du milieu
			end = mid;
		}
		// Si la valeur recherchée n'est ni plus petite, ni plus grand, donc si elle est égale à la valeur du milieu
		else
		{
			// On retourne la valeur du milieu
			return mid;
		}
	}
	return foundIndex;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
