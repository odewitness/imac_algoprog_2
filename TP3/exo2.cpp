#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */
void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
	// do not use increments, use two different binary search loop
	// deux boucles while : regarder dans la partie gauche et dans la partie droite
	
	// Boucle pour la partie de gauche
	int start = 0;
	int end = array.size();
	int foundIndex = -1;

	while (start < end)
	{
		// On initialise le milieu
		int mid = (start+end)/2;

		// Si la valeur recherchée est plus grande que la valeur du milieu
		if (toSearch > array[mid])
		{
			// On se décale vers la droite et le nouveau start est égal à la valeur située après l'ancienne valeur du milieu
			start = mid + 1;
		}
		// Si la valeur recherchée est plus petite que la valeur du milieu
		//else if (toSearch < array[mid])
		//{
			// On se décale vers la gauche et le nouveau end est égal à l'ancienne valeur du milieu
		//	end = mid;
		//}
		// Si la valeur recherchée n'est ni plus petite, ni plus grande, donc si elle est égale à la valeur du milieu
		else
		{
			// On décale le end vers la gauche
			end = mid;			
		}
	}
	indexMin=start;

	// Boucle pour la partie de droite
	// On réinitialise le start et le end
	start = 0;
	end = array.size();
	while (start < end)
	{
		// On initialise le milieu
		int mid = (start+end)/2;

		// Si la valeur recherchée est plus grande que la valeur du milieu
		if (toSearch > array[mid])
		{
			// On se décale vers la droite et le nouveau start est égal à la valeur située après l'ancienne valeur du milieu
			start = mid + 1;
		}
		// Si la valeur recherchée est plus petite que la valeur du milieu
		else if (toSearch < array[mid])
		{
			// On se décale vers la gauche et le nouveau end est égal à l'ancienne valeur du milieu
			end = mid;
		}
		// Si la valeur recherchée n'est ni plus petite, ni plus grande, donc si elle est égale à la valeur du milieu
		else
		{
			// On décale le start vers la droite
			start = mid + 1;
		}
	}
	indexMax=start-1;

	//return foundIndex;

    
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
