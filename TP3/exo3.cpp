#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow *w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = this->right = NULL;
    }

    void insertNumber(int value)
    {
        // create a new node and insert it in right or left child

        // If the data to be inserted is lesser or equal, we need to insert it in the left subtree of root, and if the data to be inserted is greater, we need to insert it in the right subtree of the root : we can reduce this problem in a recursive manner

        // A DEMANDER A ENGUERRAND
        // Inférieur ou inférieur ou égal ?
        // Réponse : en général on n'ajoute pas deux fois le même nombre

        if (value == this->value)
        {
            printf("Erreur, valeur déjà presente");
            return;
        }
        // Si la valeur à ajouter est inférieure à la valeur actuelle
        else if (value < this->value)
        {
            // S'il n'y a pas de noeud à gauche
            if (this->left == NULL)
            {
                // La valeur à ajouter devient le premier noeud à gauche
                this->left = new SearchTreeNode(value);
            }
            // Sinon
            else
            {
                // On insère la valeur à gauche
                this->left->insertNumber(value);
            }
        }
        else
        {
            // S'il n'y a pas de noeud à droite
            if (this->right == NULL)
            {
                // La valeur à ajouter devient le premier noeud à droite
                this->right = new SearchTreeNode(value);
            }
            // Sinon
            else
            {
                // On insère la valeur à droite
                this->right->insertNumber(value);
            }
        }
    }

    uint height() const
    {
        // should return the maximum height between left child and right child +1 for itself.
        // If there is no child, return just 1

        // La hauteur d'un arbre s'agit du plus grand chemin de la racine à une feuille
        // On remarque qu'il s'agit du maximum entre les sous-arbres de gauche et de droite + 1
        // Puisque la hauteur de l'arbre est la hauteur maximale du sous-arbre + 1, on continue à calculer jusqu'à ce que le sous-arbre devienne NULL

        // A DEMANDER A ENGUERRAND
        // Est-ce qu'on fait des return pour la comparaison entre les deux arbres ?

        uint left_height = 0;
        uint right_height = 0;

        // S'il n'y a pas d'enfant, donc s'il n'y a ni de noeud à droite, ni de noeud à gauche
        if (isLeaf())
        {
            // On retourne 1
            return 1;
        }

        // S'il n'y a pas de noeud à gauche
        if (this->left == NULL)
        {
            // Le compteur reste à zéro
            left_height = 0;
        }
        // Sinon
        else
        {
            // On incrémente le compteur
            left_height += this->left->height();
        }

        // S'il n'y a pas de noeud à droite
        if (this->right == NULL)
        {
            // Le compteur reste à zéro
            right_height = 0;
        }
        // Sinon
        else
        {
            // On incrémente le compteur
            right_height += this->right->height();
        }

        if (left_height >= right_height)
        {
            return left_height + 1;
        }
        else
        {
            return right_height + 1;
        }
    }

    uint nodesCount() const
    {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        // A DEMANDER A ENGUERRAND
        // On démarre le compteur à 0 ou à 1 ?
        // Réponse : à 1 car on compte le noeud actuel

        uint count = 1;

        if (isLeaf())
        {
            return 1;
        }

        if (this->left != NULL)
        {
            count += this->left->nodesCount();
        }

        if (this->right != NULL)
        {
            count += this->right->nodesCount();
        }
        return count;
    }

    bool isLeaf() const
    {
        // return True if the node is a leaf (it has no children)
        // pas optimal
        /*if (this->left == NULL && this->right == NULL)
        {
            return true;
        }
        else
        {
            return false;
        }*/

        return (this->left == NULL && this->right == NULL);
    }

    void allLeaves(Node *leaves[], uint &leavesCount)
    {
        // fill leaves array with all leaves of this tree

        // leavesCount : nombre de feuilles qui restent = index d'ajout dans le tableau

        // Si on arrive sur une feuille
        // On ajoute le compteur dans le tableau
        // On le décrémente
        //return;
        //printf("test");

        if (this->isLeaf())
        {
            leaves[leavesCount] = this; // pointeur vers soit même
            leavesCount++;
        }
        // Sinon
        else
        {
            if (this->left != NULL)
            {
                left->allLeaves(leaves, leavesCount);
            }
            if (this->right != NULL)
            {
                right->allLeaves(leaves, leavesCount);
            }
        }
    }

    void inorderTravel(Node *nodes[], uint &nodesCount)
    {
        // fill nodes array with all nodes with inorder travel

        // On commence tout en haut
        // On va dans le sous arbre à gauche
        // S'il n'y a plus d'enfant à gauche, on retourne le noeud
        // On essaie de passer à droite, mais il n'y a pas d'enfant non plus, donc on retourne au noeud d'avant
        // On va dans le sous arbre à droite

        // Fils gauche
        if (this->left != NULL)
        {
            left->inorderTravel(nodes, nodesCount);
        }

        // Parent
        nodes[nodesCount] = this; // pointeur vers soit même
        nodesCount ++;

        // Fils droite
        if (this->right != NULL)
        {
            right->inorderTravel(nodes, nodesCount);
        }
    }

    void preorderTravel(Node *nodes[], uint &nodesCount)
    {
        // Parent
        nodes[nodesCount] = this; // pointeur vers soit même
        nodesCount ++;
        
        // Fils gauche
        if (this->left != NULL)
        {
            left->preorderTravel(nodes, nodesCount);
        }

        // Fils droit
        if (this->right != NULL)
        {
            right->preorderTravel(nodes, nodesCount);
        }
    }

    void postorderTravel(Node *nodes[], uint &nodesCount)
    {
        // fill nodes array with all nodes with postorder travel     

        // Fils gauche
        if (this->left != NULL)
        {
            left->postorderTravel(nodes, nodesCount);
        }

        // Fils droit
        if (this->right != NULL)
        {
            right->postorderTravel(nodes, nodesCount);
        }

        // Parent
        nodes[nodesCount] = this; // pointeur vers soit même
        nodesCount ++;
    }

    Node *find(int value)
    {
        // find the node containing value

        // Si la valeur recherchée est égale à la valeur où on est
        if (value == this->value)
        {
            // On retourne la valeur
            return this;
        }
        else
        {
            // Si la valeur recherchée est inférieure à la valeur où on est
            if (value < this->value)
            {
                // On vérifie qu'on ne soit pas à la fin
                if (this->left != NULL)
                {
                    // On part vers la gauche
                    left->find(value);
                }
            }
            // Si la valeur recherchée est supérieure à la valeur où on est
            else
            {
                // On vérifie qu'on ne soit pas à la fin
                if (this->right != NULL)
                {
                    // On part vers la droite
                    right->find(value);
                }
            }
        }
        //return nullptr;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

    return a.exec();
}
