#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

	for (int i = 0; i < nodeCount; i++)
	{
		this->appendNewNode(new GraphNode(i));
	}

	// à partir de chaque noeud, on regarde pour chaque noeud destination s'il y a un lien, si les deux sont adjecent on ajoute une arete au noeud i qui va vers j
	for (int i =0; i < nodeCount; i++)
	{
		for (int j=0; j<nodeCount; j++)
		{
			if(adjencies[i][j])
			{
				this->nodes[i]->appendNewEdge();
			}
		}
	}
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */

	// leurs valeurs sont les indices

	// s'il n'a pas été visité
	if (!visited[first->value])
	{
		// on dit qu'il a été visité 
		visited[first->value] = 1;
		nodes[nodesSize] = first;
		// pour pouvoir aller voir tous les enfants, on part de la première
		// liste chainée
		Edge edge = first->edges;
		// tant que l'arrête n'est pas nulle
		while(edge != NULL)
		{
			// on rappelle le deeptravel
			deepTravel(edge.destination, nodes, nodesSize+1, visited);
			// on passe au suivant
			edge=edge.next;
		}
	}
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */

	std::queue<GraphNode*> nodeQueue;
	//on push le premier noeud
	nodeQueue.push(first);
	GraphNode* node;
	int index = 0;
	Edge edge;
	//tant que la taille de la file n'est pas nulle
	while (nodeQueue.size()!=0)
	{
		// on sort le premier noeud
		node = nodeQueue.front(); // récupère le premier élément 
		nodeQueue.pop(); // enlève le premier élément de la liste
		visited[node->value] = true;
		nodes[index++] = node; 
		edge = node->edges; 
		while(edge!=NULL)
		{
			if (!visited[edge->destination])
			{
				// on ajoute dans la file toutes les destinations des arêtes
				nodeQueue.push(edge.destination);
				edge = edge.next;
			}
		}
	}
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/

	visited[first->value] = true;
	// Edge* e = first->edges;
	// while (e!=NULL)
	for(Edge *e = first->edges; e!=nullptr; e=e->next)
	{
		if(!visited[e->destination->value])
		{
			if(detectCycle(e->destination, visited))
			{
				return true;
			}
		}
		else
		{
			return true;
		}
		// e=e->next;
	}
    return false;
	
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
